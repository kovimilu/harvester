#include "harvester.h"
#include <iostream>
#include <string>
#include <Windows.h>
#include <TlHelp32.h>

using namespace std;

DWORD GetParentProcessID()
{
    DWORD currentProcessID = GetCurrentProcessId();
    HANDLE snapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
    PROCESSENTRY32 processEntry;
    processEntry.dwSize = sizeof(PROCESSENTRY32);

    if (Process32First(snapshot, &processEntry))
    {
        do
        {
            if (processEntry.th32ProcessID == currentProcessID)
            {
                CloseHandle(snapshot);
                return processEntry.th32ParentProcessID;
            }
        } while (Process32Next(snapshot, &processEntry));
    }
    CloseHandle(snapshot);
    return 0;
}

string GetProcessName(DWORD processID)
{
    HANDLE snapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
    PROCESSENTRY32 processEntry;
    processEntry.dwSize = sizeof(PROCESSENTRY32);

    if (Process32First(snapshot, &processEntry))
    {
        do
        {
            if (processEntry.th32ProcessID == processID)
            {
                CloseHandle(snapshot);
                return processEntry.szExeFile;
            }
        } while (Process32Next(snapshot, &processEntry));
    }
    CloseHandle(snapshot);
    return "";
}

int main(int argc, char *argv[])
{
    string output_filename = "_nolidzs.txt";
    Harvester harvester(output_filename);

    for (int i = 1; i < argc; ++i)
    {
        string arg = argv[i];
        if (arg == "--about")
        {
            harvester.display_about();
            return EXIT_SUCCESS;
        }
        else if (arg == "--help")
        {
            harvester.display_help();
            return EXIT_SUCCESS;
        }
        else if (arg == "--output")
        {
            if (i + 1 < argc)
            {
                output_filename = argv[++i];
                harvester = Harvester(output_filename);
            }
            else
            {
                cerr << "--output requires an argument" << endl;
                return EXIT_FAILURE;
            }
        }
    }

    harvester.process_files();

    DWORD parentProcessID = GetParentProcessID();
    string parentProcessName = GetProcessName(parentProcessID);

    if (parentProcessName == "explorer.exe")
    {
        system("pause");
    }

    return EXIT_SUCCESS;
}

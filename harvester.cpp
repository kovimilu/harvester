#include "harvester.h"
#include <iostream>
#include <fstream>
#include <string>
#include <dirent.h>
using namespace std;

Harvester::Harvester(const string& output_filename) : output_filename(output_filename) {}

void Harvester::display_about() const
{
    cout << "Harvester 3.0\n"
         << "This program reads all text files in the current directory and appends"
         << "their content into a single output file called "
         << output_filename << " by default.\n";
}

void Harvester::display_help() const
{
    cout << "Usage: program [options]\n\n"
         << "Options:\n"
         << "  --about\t\tDisplay information about the program.\n"
         << "  --help\t\tDisplay this help message.\n"
         << "  --output <filename>\tSpecify the output file name (default: " << output_filename << ").\n";
}

void Harvester::process_files() const
{
    ofstream ki(output_filename, fstream::app);

    DIR* dir;
    struct dirent* entry;

    if ((dir = opendir(".")) != nullptr)
    {
        while ((entry = readdir(dir)) != nullptr)
        {
            string file_name = entry->d_name;

            if (file_name == output_filename || file_name == "." || file_name == "..")
                continue;

            ifstream be(file_name);
            string line;
            while (getline(be, line))
            {
                cout << line << endl;
                ki << line << endl;
            }
            be.close();
        }
        closedir(dir);
    }
    else
    {
        perror("Error opening directory");
    }

    ki.close();
}

# Harvester 3.0

Harvester 3.0 is a simple command-line utility that reads all text files in the current directory and appends their content into a single output file. By default, the output file is named _nolidzs.txt. However, you can specify a custom output file name using the `--output` flag.

This tool was developed with the help of ChatGPT.

## Features

- Combine all text files in the current directory into one output file
- Specify a custom output file name using the `--output` flag
- Display information about the program using the `--about` flag
- Display a help message with usage instructions using the `--help` flag
- Pause the program at the end if launched from Windows File Explorer

## Prerequisites

- A C++ compiler (e.g., GCC, Clang, or Visual Studio)
- C++ Standard Library
- Windows operating system (due to Windows-specific code)

## Compilation

To compile the project, use the following command in the terminal:

```cmd
g++ -o harvester main.cpp Harvester.cpp

```

## Usage

This will generate an executable named harvester (or harvester.exe on Windows).

### Options

- `--about` - Display information about the program.
- `--help` - Display the help message.
- `--output <filename>` - Specify the output file name (default: `_nolidzs.txt`).

To run the program, simply execute the compiled binary. For example, in the terminal, type:

```cmd
harvester.exe

```

To specify a custom output file name, use the --output flag followed by the file name:

```cmd
harvester.exe --output custom_output.txt

```

To display information about the program, use the --about flag:

```cmd
harvester.exe --about

```

To display a help message with usage instructions, use the --help flag:

```cmd
harvester.exe --help

```

License

This project is open-source and available under the MIT License.
Contributing

If you'd like to contribute to the project, please submit a pull request or create an issue for discussion. Contributions are always welcome!
#pragma once
#include <string>

class Harvester
{
public:
    Harvester(const std::string& output_filename = "_nolidzs.txt");
    void display_about() const;
    void display_help() const;
    void process_files() const;

private:
    std::string output_filename;
};
